<?php

namespace App\Service;

use Doctrine\Persistence\ManagerRegistry;
use Port\Csv\CsvReader;
use Port\Doctrine\DoctrineWriter;
use Port\Reader\ArrayReader;
use Port\Reader\CountableReader;
use Port\Steps\StepAggregator;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Port\Filter\ValidatorFilter;
use Symfony\Component\Validator\Constraints as Assert;

class TestTakerService
{
    private $em;
    private $validator;

    public function __construct(ManagerRegistry $managerRegistry, ValidatorInterface $validator)
    {
        $this->em = $managerRegistry->getManager();
        $this->validator = $validator;
    }

    public function importData($path): void
    {
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        switch ($ext) {
            case 'json':
                $this->readJsonToDataBase($path);
                break;
            case 'csv':
            default:
                $this->readCsvToDataBase($path);
        }
    }

    private function readCsvToDataBase($path): void
    {
        $file = new \SplFileObject($path);
        $csvReader = new CsvReader($file);
        $csvReader->setHeaderRowNumber(0);

        $this->processData($csvReader);
    }

    private function readJsonToDataBase($file): void
    {
        $strJsonFileContents = file_get_contents($file);
        $array = json_decode($strJsonFileContents, true);
        $reader = new ArrayReader($array);

        $this->processData($reader);
    }

    private function processData(CountableReader $reader): void
    {
        $workflow = new StepAggregator($reader);
        $doctrineWriter = new DoctrineWriter($this->em, 'App\Entity\TestTaker');
        $workflow->addWriter($doctrineWriter);

        $workflow->process();
    }
}
