<?php

namespace App\Repository;

use App\Entity\TestTaker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method TestTaker|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestTaker|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestTaker[]    findAll()
 * @method TestTaker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestTakerRepository extends ServiceEntityRepository
{
    public const ITEMS_PER_PAGE = 10;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestTaker::class);
    }

    public function getTestTakerPaginator(int $offset, int $limit, string $filter = null): Paginator
    {
        $qb = $this->createQueryBuilder('tt');
        $qb
            ->setMaxResults(self::ITEMS_PER_PAGE)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;
        if ($filter) {
            $qb
                ->where(
                    $qb->expr()->orX(
                        $qb->expr()->like('tt.lastname', ':lastname'),
                        $qb->expr()->like('tt.firstname', ':firstname')
                    )
                )
                ->setParameters([
                    'lastname' => '%'.$filter.'%',
                    'firstname' => '%'.$filter.'%',
                ])
            ;
        }

        return new Paginator($qb->getQuery());
    }
}
