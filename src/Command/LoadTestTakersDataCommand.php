<?php

namespace App\Command;

use App\Service\TestTakerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class LoadTestTakersDataCommand extends Command
{
    protected static $defaultName = 'load:test-takers';
    private $takerService;

    public function __construct(TestTakerService $takerService)
    {
        parent::__construct();
        $this->takerService = $takerService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Load test takers list to DB.')
            ->addArgument('path_to_file', InputArgument::REQUIRED, 'Path to uploaded file.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $pathToFile = $input->getArgument('path_to_file');

        if ($pathToFile) {
            $this->takerService->importData($pathToFile);
        }
        $io->success('Data successfully loaded.');

        return 0;
    }
}
