<?php


namespace App\Controller\Api\v1;

use App\Repository\TestTakerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/v1", name="api-v1-")
 */
class TestTakerController extends AbstractController
{
    private $serializer;
    private $testTakerRepository;

    public function __construct(SerializerInterface $serializer, TestTakerRepository $testTakerRepository)
    {
        $this->serializer = $serializer;
        $this->testTakerRepository = $testTakerRepository;
    }

    /**
     * @Route("/users/", name="users-list")
     * @param Request $request
     * @return Response
     */
    public function actionList(Request $request): Response
    {
        $offset = max(0, $request->query->getInt('offset', 0));
        $limit = $request->get('limit', $this->testTakerRepository::ITEMS_PER_PAGE);
        $filter = $request->get('name');

        $paginator = $this->testTakerRepository->getTestTakerPaginator($offset, $limit, $filter);
        $testTakers = $paginator->getQuery()->getResult();

        $serializedEntity = $this->serializer->serialize($testTakers, 'json', ['groups' => 'list']);

        return new Response($serializedEntity);
    }

    /**
     * @Route("/user/{id}/", methods={"GET"}, name="read-user", requirements={"id"="\d+"}, options={"expose"=true})
     * @param Request $request
     * @return JsonResponse
     */
    public function actionRead(Request $request): JsonResponse
    {
        $id = $request->get('id');
        $testTaker = $this->testTakerRepository->find($id);

        return $this->json([
            'test_taker' => $testTaker,
        ]);
    }
}
