<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="main-page")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to Practical Exercise Client!',
        ]);
    }
}
